import React, { Component } from 'react';
import moment from 'moment';

class WeatherDetail extends Component {
    render() {
        return <div className="card">
            <div className="card-header">
              {moment(this.props.data.applicable_date).format(
                "dddd (MMM Do)"
              )}
            <img
                className="img-thumbnail float-right"
                src={`https://www.metaweather.com/static/img/weather/png/64/${this.props.data.weather_state_abbr}.png`} 
                alt={this.props.city}
                style={{ width: "64px", height: "64px" }} />
            </div>
            <div className="card-body">
              <p>Temperature right now: {Math.round(this.props.data.the_temp)}</p>
              <p>Min Temp: {Math.round(this.props.data.min_temp)}</p>
              <p>Max Temp: {Math.round(this.props.data.max_temp)}</p>
            </div>
          </div>;
    }
}

export default WeatherDetail;