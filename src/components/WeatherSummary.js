import React, { Component } from "react";
import axios from 'axios';
import { withRouter } from "react-router";
import { Link } from 'react-router-dom';

class WeatherSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            woeid: "",
            weather_data: [],
            curr_temp: "",
            min_temp: "",
            max_temp: "",
            icon: "",
            noResults: true,
            city: props.city
        }

        this.getWeatherData = this.getWeatherData.bind(this);
       
    }

    componentDidMount() {
        if (this.props.match.path.indexOf("/search") === 0 && (typeof this.props.match.params.keyword === "string")) {
            this.setState({ city: this.props.match.params.keyword });
            this.getWeatherData(this.props.match.params.keyword);
        } else if (this.props.match.path === "/") {
            this.getWeatherData(this.props.city);
        }
        //http://codeline:8888/weather.php?command=search&keyword=Dublin
        
        
    }
    componentDidUpdate() {
        if (this.props.match.path.indexOf("/search") === 0 && (typeof this.props.match.params.keyword === "string" && this.state.city !== this.props.match.params.keyword)) {
          this.setState({ city: this.props.match.params.keyword });
          this.getWeatherData(this.props.match.params.keyword);
          
        }
        
    }
    getWeatherData(keywordstr) {

        if ((typeof this.state.city === "string") && this.state.city.indexOf(" ") > 0) {
            window.apiurl_keyword = "https://www.metaweather.com/api/location/search/?query=";
            window.apiurl_woeid = "https://www.metaweather.com/api/location/";
        }
        axios
          .get(`${window.apiurl_keyword}${keywordstr}`)
          .then(response => {
            if (response.data.length > 0) {
              let title = response.data[0].title;
              let woeid = response.data[0].woeid;

              return axios
                .get(`${window.apiurl_woeid}${woeid}`)
                .then(response => {
                  this.setState({
                    title,
                    woeid,
                    weather_data: response.data,
                    curr_temp: Math.round(
                      response.data.consolidated_weather[0].the_temp
                    ),
                    min_temp: Math.round(
                      response.data.consolidated_weather[0].min_temp
                    ),
                    max_temp: Math.round(
                      response.data.consolidated_weather[0].max_temp
                    ),
                    icon: `https://www.metaweather.com/static/img/weather/png/64/${
                      response.data.consolidated_weather[0]
                        .weather_state_abbr
                    }.png`,
                    noResults: false
                  });
                });
            } else {
              this.setState({ noResults: true });
            }
          })
          .catch(function(error) {
            // handle error
            console.log(error);
          });
    }

    
    render() {
        if (this.state.noResults && this.props.location.pathname.indexOf("/search") === 0) {
            return <div className="text-alert">
                No Results were found. Try changing the keyword!
              </div>;
        }
        return <div className="card">
            <div className="card-header">
              {this.state.city}
              {(this.state.icon.length > 0)?
                <img className="img-thumbnail float-right" src={this.state.icon} alt={this.props.city} style={{width: "64px", height: "64px"}}/>
                :
                <div className="float-right ph-item ph-picture" style={{width: "64px", height: "64px"}}></div>
              }
            </div>
            <div className="card-body">
              {this.state.curr_temp && this.state.min_temp && this.state.max_temp && <div>
                    <p>Temperature right now: {this.state.curr_temp}</p>
                    <p>Min Temp: {this.state.min_temp}</p>
                    <p>Max Temp: {this.state.max_temp}</p>
                  </div>}
            </div>
            <div className="card-footer text-center">
            <Link to={`/weather/${this.state.woeid}`}>More Details</Link>
            </div>
          </div>;
    }
}


export default withRouter(WeatherSummary);