import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  NavLink
} from "react-router-dom";

import Home from './pages/Home';
import Search from "./pages/Search";
import Weather from "./pages/Weather";


import '../App.css';

class App extends Component {
  render() {
    return <Router>
        <div>
          <div className="bg-primary">
            <Nav />
          </div>
          <div className="container">
            <Route exact path="/" component={Home} />
            <Route path="/search/:keyword?" component={Search} />
            <Route path="/weather/:woeid?" component={Weather} />
          </div>
          <div className="content" />
        </div>
      </Router>;
  }
}

const Nav = () => (
  <div className="container">
    <nav className="navbar navbar-expand-lg navbar-dark">
      <NavLink className="navbar-brand" to="/">
        Weather App
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink
            exact
            className="nav-item nav-link"
            activeClassName="active"
            to="/"
          >
            Home
          </NavLink>
          <NavLink
            exact
            className="nav-item nav-link"
            activeClassName="active"
            to="/search/"
          >
            Search
          </NavLink>
        </div>
      </div>
    </nav>
  </div>
);

export default App;
