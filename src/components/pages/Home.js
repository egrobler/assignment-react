import React, {Component} from "react";
import WeatherSummary from "../WeatherSummary";
import SearchWidget from "../SearchWidget";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "cities": ["Istanbul", "Berlin", "London", "Helsinki", "Dublin", "Vancouver"]
        }
    }

    render() {
        return <div>
                <SearchWidget />
                <div className="card">
                    <div className="card-header">
                        Currently Displaying weather for:
                </div>
                    <div className="card-body">
                        <div className="row">
                            {this.state.cities.map((city, i) => {
                                return <div className="col-md-4 mb-4" key={i}><WeatherSummary city={city} /></div>
                            })}
                        </div>
                    </div>
                </div>
            </div>;
    }
}


export default Home;