import React, { Component } from "react";
import WeatherSummary from "../WeatherSummary";
import SearchWidget from "../SearchWidget";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = { city: props.match.params.keyword };
    }
    componentDidMount() {
        
    }
    render() {
        return <div>
                <SearchWidget city={this.props.match.params.keyword} />
                <div className="card">
                <div className="card-header">
                    Currently Displaying weather for:
                </div>
                <div className="card-body">
                    <WeatherSummary city={this.state.city} />
                </div>
                </div>
            </div>;
    }
}


export default Search;