import React, { Component } from "react";
import axios from "axios";
import WeatherDetail from '../WeatherDetail';

class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            weather_data: [],
            city: ""
        }
        this.getWeatherData = this.getWeatherData.bind(this);
    }
    componentDidMount() {
        this.getWeatherData();
    }

    getWeatherData() {

        axios
            .get(`${window.apiurl_woeid}${this.props.match.params.woeid}`)
            .then(response => {
                if (typeof response.data.consolidated_weather === "object" && response.data.consolidated_weather.length > 0) {
                    this.setState({
                        city: response.data.title,
                      weather_data:
                        response.data.consolidated_weather
                    });
                }
            });
    }
    render() {

        if (this.state.city.length) {
            return <div className="mt-3">
                <div className="card">
                    <div className="card-header">
                        Detailed Weather for: {(this.state.city.length) ? this.state.city : '...'}
                    </div>
                    <div className="card-body">
                        <div className="row">
                            {this.state.weather_data.map((data, i) => {
                                return <div className="col-md-4 mb-4" key={i}><WeatherDetail data={data} city={this.state.city} /></div>
                            })}
                        </div>
                    </div>
                </div>
            </div>;
        } else {
            return <div className="row mt-3">
                <div className="col-md-12 text-center">
                  Loading...
                </div>
              </div>;
        }


        
    }
}


export default Weather;