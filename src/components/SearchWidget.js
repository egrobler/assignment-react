import React, {Component} from 'react';
import { withRouter } from "react-router";

class SearchWidget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputCityName: ((typeof props.city === "string") ? this.props.city : ''),
            toSearchPage: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({ inputCityName: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            toSearchPage: true
        })
        this.props.history.push({
          pathname: `/search/${this.state.inputCityName}`
        });
    }

    
    render() {
        return (
            <div className="card mt-3 mb-3">
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-auto">
                            <h1 className="h4">Get weather for a city:</h1>
                        </div>
                        <div className="col-md-auto">
                                <form className="form-inline" onSubmit={this.handleSubmit}>
                                    <label className="sr-only" htmlFor="inputCityName">
                                        City Name
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control mb-2 mr-sm-2"
                                        id="inputCityName"
                                        placeholder="City Name"
                                        value={this.state.inputCityName}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <button type="submit" className="btn btn-primary mb-2">
                                        Search
                                    </button>
                                </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}
 
export default withRouter(SearchWidget);